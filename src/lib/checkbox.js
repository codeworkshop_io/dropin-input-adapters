import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";

export default props => {
  const {
    input,
    disabled,
    label,
    formControlProps,
    meta: { touched, error },
    inputProps
  } = props;
  return (
    <FormControl {...formControlProps}>
      <FormControlLabel
        control={
          <Checkbox
            checked={!!input.value}
            onChange={input.onChange}
            disabled={disabled}
          />
        }
        label={label}
      />
      {((inputProps && inputProps.helperText) || (touched && !!error)) && (
        <FormHelperText>{error || inputProps.helperText}</FormHelperText>
      )}
    </FormControl>
  );
};
