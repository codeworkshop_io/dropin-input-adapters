import React from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

export default props => {
  const {
    meta: { touched, error, warning },
    items,
    label,
    formControlProps,
    underlined,
    InputLabelProps,
    ...rest
  } = props;
  return (
    <TextField
      select
      label={label}
      {...rest}
      error={!!touched && !!error}
      helperText={touched && (error || warning)}
      value={props.input.value || ""}
      onChange={props.input.onChange}
      InputLabelProps={{
        ...InputLabelProps
      }}
    >
      {items &&
        Array.isArray(items) &&
        items.length > 0 && [
          <MenuItem key="" value="">
            -- None --
          </MenuItem>,
          ...items.map(x => (
            <MenuItem key={x.value || x} value={x.value || x}>
              {x.label || x}
            </MenuItem>
          ))
        ]}
    </TextField>
  );
};
