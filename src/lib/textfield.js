import React from "react";
import TextField from "@material-ui/core/TextField";

export default props => {
  const {
    input: { value, ...input },
    meta: { touched, invalid, error, warning },
    ...rest
  } = props;
  return (
    <TextField
      error={touched && invalid}
      helperText={touched && (error || warning)}
      value={value || ""}
      {...input}
      {...rest}
    />
  );
};
