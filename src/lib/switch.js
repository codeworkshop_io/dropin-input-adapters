import React from "react";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";

export default props => {
  const {
    input,
    label,
    inputProps,
    formControlProps,
    meta: { touched, error },
    disabled
  } = props;
  return (
    <FormControl {...formControlProps}>
      <FormControlLabel
        control={
          <Switch
            checked={!!input.value}
            onChange={input.onChange}
            disabled={disabled}
          />
        }
        label={label}
      />
      {((inputProps || {}).helperText || (touched && !!error)) && (
        <FormHelperText>{error || inputProps.helperText}</FormHelperText>
      )}
    </FormControl>
  );
};
