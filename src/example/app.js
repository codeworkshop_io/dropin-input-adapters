import React, { useState } from "react";

import { Paper, Typography, Grid } from "@material-ui/core";

import {
  SelectAdapter,
  TextAdapter,
  SwitchAdapter,
  CheckboxAdapter
} from "../lib";

const Example = () => {
  const [inputValues, setInputValues] = useState({});

  const setValue = name => event => {
    setInputValues({
      ...inputValues,
      [name]:
        typeof event === "object"
          ? event.target.value || event.target.checked
          : event
    });
  };

  return (
    <Paper style={{ padding: "20px" }}>
      <Typography variant="h4">Input Example page</Typography>
      <hr />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <SelectAdapter
            meta={{}}
            input={{ value: inputValues.select, onChange: setValue("select") }}
            fullWidth
            label="Select"
            items={["test1", "test2", "test3", "test4"]}
          />
        </Grid>
        <Grid item xs={12}>
          <TextAdapter
            label="Text"
            meta={{}}
            input={{ value: inputValues.text, onChange: setValue("text") }}
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <SwitchAdapter
            label="Switch"
            meta={{}}
            input={{ value: inputValues.switch, onChange: setValue("switch") }}
          />
        </Grid>
        <Grid item xs={12}>
          <CheckboxAdapter
            label="Checkbox"
            meta={{}}
            input={{
              value: inputValues.checkbox,
              onChange: setValue("checkbox")
            }}
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <Paper style={{ padding: "10px" }}>
            <pre>{JSON.stringify(inputValues, null, 2)}</pre>
          </Paper>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default Example;
