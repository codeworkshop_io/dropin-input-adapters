# Dropin Input Adapters

Preconfigured input adapters for reactjs redux-form field components.

## Installation

`npm install git+ssh://git@bitbucket.org:codeworkshop_io/dropin-input-adapters.git`

**Remember: You'll need to have git & ssh configured on your local machine to install
from bitbucket.**

## Usage

Import the adapter you want and set it as the component property on the
redux-form field.

```javascript
import { TextFieldAdapter } from "dropin-input-adapters"

export default const props => {
    return (
        <form>
            <Field name="one-field" component={TextFieldAdapter}/>
        </form>
    )
}
```

## Deploying

Not sure how NPM handles this, but you need to build the project and have the built 
result in the repository. Just use `npm run-script build` and commit the result.

Sometimes building goes crazy so you might need to do the following for some unknown reason (will look into it)

```
npm i react react-dom react-scripts  @babel/core @babel/cli @babel/preset-react 
```

## License

Some license should be selected. This is however property of codeworkshop.io

You can probably also look at https://github.com/erikras/redux-form-material-ui
