function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
export default (props => {
  const {
    meta: {
      touched,
      error,
      warning
    },
    items,
    label,
    formControlProps,
    underlined,
    InputLabelProps,
    ...rest
  } = props;
  return React.createElement(TextField, _extends({
    select: true,
    label: label
  }, rest, {
    error: !!touched && !!error,
    helperText: touched && (error || warning),
    value: props.input.value,
    onChange: props.input.onChange,
    InputLabelProps: { ...InputLabelProps
    }
  }), items && Array.isArray(items) && items.length > 0 && [React.createElement(MenuItem, {
    value: ""
  }, "-- None --"), ...items.map(x => React.createElement(MenuItem, {
    value: x.value || x
  }, x.label || x))]);
});