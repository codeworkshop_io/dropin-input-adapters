import React from "react";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
export default (props => {
  const {
    input,
    label,
    inputProps,
    formControlProps,
    meta: {
      touched,
      error
    },
    disabled
  } = props;
  return React.createElement(FormControl, formControlProps, React.createElement(FormControlLabel, {
    control: React.createElement(Switch, {
      checked: !!input.value,
      onChange: input.onChange,
      disabled: disabled
    }),
    label: label
  }), ((inputProps || {}).helperText || touched && !!error) && React.createElement(FormHelperText, null, error || inputProps.helperText));
});