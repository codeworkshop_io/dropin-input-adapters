import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
export default (props => {
  const {
    input,
    disabled,
    label,
    formControlProps,
    meta: {
      touched,
      error
    },
    inputProps
  } = props;
  return React.createElement(FormControl, formControlProps, React.createElement(FormControlLabel, {
    control: React.createElement(Checkbox, {
      checked: !!input.value,
      onChange: input.onChange,
      disabled: disabled
    }),
    label: label
  }), (inputProps && inputProps.helperText || touched && !!error) && React.createElement(FormHelperText, null, error || inputProps.helperText));
});