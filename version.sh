tags="$(git ls-remote --tags | grep -v v| grep -v -|grep -v {| sort -t '/' -k 3 -V -r | awk -F/ '{ print $3 }' | head -n 1)"

echo "$tags" > "./version.txt"

file="./version.txt"
while IFS="." read -r major minor patch
do
    patch="$(echo "$patch" | cut -d'-' -f1)"
    
    printf 'Current - Major: %s, Minor: %s, Patch: %s\n' "$major" "$minor" "$patch"

    nextPatch=$((patch + 1))

    printf 'Next - Major: %s, Minor: %s, Patch: %s\n' "$major" "$minor" "$nextPatch"

    export NEXT_VERSION="$major.$minor.$nextPatch"

    # echo "${NEXT_VERSION}" > version.txt
done <"$file"